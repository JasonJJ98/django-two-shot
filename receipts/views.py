from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import (ReceiptForm, CategoryForm,
                            AccountForm)

# Create your views here.

@login_required
def ReceiptList(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipt,
    }

    return render(request, "receipts/receipt_list.html", context)

@login_required
def CreateReceipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)

@login_required
def CategoryList(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": category,
    }
    return render(request, "receipts/category_list.html", context)

@login_required
def CreateCategory(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def AccountList(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "accounts": account,
    }
    return render(request, "receipts/account_list.html", context)

@login_required
def CreateAccount(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
