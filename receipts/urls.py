from django.urls import path
from receipts.views import (ReceiptList, CreateReceipt, CategoryList,
                            AccountList, CreateCategory, CreateAccount)

urlpatterns = [
    path("", ReceiptList, name="home"),
    path("create/", CreateReceipt, name="create_receipt"),
    path("categories/", CategoryList, name="category_list"),
    path("accounts/", AccountList, name="account_list"),
    path("categories/create/", CreateCategory, name="create_category"),
    path("accounts/create/", CreateAccount, name="create_account"),
]
